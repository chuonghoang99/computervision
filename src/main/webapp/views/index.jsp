<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>Document</title>


    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/lib/bootstrap.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/lib/font-awesome.min.css">

    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/style.css">

    <%--    <script src="${pageContext.request.contextPath}/resources/lib/bootstrap.bundle.min.js"></script>--%>
    <script src="${pageContext.request.contextPath}/resources/lib/jquery-3.3.1.min.js"></script>

    <style>
        .loadingForm {
            display: block;
            position: fixed;
            z-index: 1000;
            top: 0;
            left: 0;
            height: 100%;
            width: 100%;
            background: rgba(255, 255, 255, .8) url('../resources/lib/loading.svg') 50% 50% no-repeat;
        }
    </style>
</head>
<body>
<div id="loadingForm" ></div>
<div class="container py-5">

    <!-- For demo purpose -->
    <header class="text-white text-center">
        <h3 class="display-5">COMPUTER VISION</h3>
        <div class="mt-4"><img id="logoBK" src="/resources/lib/hust.svg" alt=""
                                          class="img-fluid rounded shadow-sm mx-auto d-block" style="max-height: 150px"></div>

    </header>

    <div class="row py-4">
        <div class="col-lg-6 mx-auto">
            <form id="formUploadImage">
                <!-- Upload image input-->
                <div class="input-group mb-3 px-2 py-2 rounded-pill bg-white shadow-sm">
                    <input id="upload" type="file" name="file" onchange="readURL(this);" class="form-control border-0">
                    <label id="upload-label" for="upload" class="font-weight-light text-muted">Choose file</label>
                    <div class="input-group-append">
                        <label for="upload" class="btn btn-light m-0 rounded-pill px-4"><small
                                class="text-uppercase font-weight-bold text-muted">Choose file</small></label>
                    </div>
                </div>


                <div class="image-area mt-4"><img id="imageResult" src="#" alt=""
                                                  class="img-fluid rounded shadow-sm mx-auto d-block"></div>

                <button class="btn btn-secondary mt-3 d-block mx-auto">Upload</button>

                <div id="image-after">

                </div>
            </form>


        </div>


    </div>


</div>

<script>

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#imageResult')
                    .attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }

    $(function () {
        $('#upload').on('change', function () {
            readURL(input);
        });
    });

    let input = document.getElementById('upload');
    let infoArea = document.getElementById('upload-label');

    input.addEventListener('change', showFileName);

    function showFileName(event) {
        let input = event.srcElement;
        let fileName = input.files[0].name;
        infoArea.textContent = 'File name: ' + fileName;
    }

</script>


<script src="${pageContext.request.contextPath}/resources/index.js"></script>
</body>
</html>
