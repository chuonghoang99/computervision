$(document).ready(function () {
    $(document).on('submit', '#formUploadImage', function (e) {
        e.preventDefault();
        console.log("submit")
        let loading = $("#loadingForm");
        loading.addClass('loadingForm');

        let formData = new FormData($('#formUploadImage')[0]);

        for (let value of formData.values()) {
            if (value.name === '') {
                loading.removeClass('loadingForm');
                return false;
            }
        }

        $.post({
            async: true,
            url: "/api/upload/image",
            processData: false,
            contentType: false,
            data: formData,
            success: function (response) {
                console.log("success")
                loading.removeClass('loadingForm');
                JSON.parse(JSON.stringify(response))
                console.log(response)
                console.log(response.data)
                if (response.status === 'SUCCESS') {
                    $('#image-after').html(`<div class="image-area mt-4">
                    <img id="imageAfter" src="/resources/output_image/${response.data}"
                         class="img-fluid rounded shadow-sm mx-auto d-block"></div>`)
                }
            },
            error: function (response) {
                loading.removeClass('loadingForm');
                JSON.parse(JSON.stringify(response))
            }
        })

    });
});

