package fa.appcode;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ComputerVision {

	public static void main(String[] args) {
		SpringApplication.run(ComputerVision.class, args);
	}

}
