package fa.appcode.services;



import fa.appcode.common.logging.LogUtils;
import fa.appcode.exceptions.ImageFileException;

import org.apache.commons.io.FilenameUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Arrays;
import java.util.Objects;
import java.util.UUID;

@Service
public class ImageService  {

    private boolean isImageFile(MultipartFile file) {
        String fileExtension = FilenameUtils.getExtension(file.getOriginalFilename());
        if (fileExtension == null) {
            return false;
        }
        return Arrays.asList(new String[]{"png", "jpg", "jpeg", "bmp"}).contains(fileExtension.trim().toLowerCase());
    }


    public String storeFile(MultipartFile file, String storageFolder) {

        if (file.isEmpty()) {
            throw new ImageFileException("Failed to store empty file.");
        }
        if (!isImageFile(file)) {
            throw new ImageFileException("You can only upload image file (png, jpg, jpeg, bmp)");
        }
//            //file must be <=5MB
//            float fileSizeInMegabytes = file.getSize() / 1_000_000;
//            if (fileSizeInMegabytes > 5.0f) {
//                throw new ImageFileException("Fail must be <= 5MB");
//            }

        Path uploadPath = Paths.get(storageFolder);
        Path destinationFilePath = uploadPath.resolve(Paths.get(Objects.requireNonNull(file.getOriginalFilename()))).normalize().toAbsolutePath();
        try (InputStream inputStream = file.getInputStream()) {
            Files.copy(inputStream, destinationFilePath, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            LogUtils.getLogger().info("Failed to store file " + e.getMessage());
            throw new ImageFileException("Failed to store file. " + e);
        }

        return Objects.requireNonNull(file.getOriginalFilename());

    }



}
