package fa.appcode.common.utils;

import java.util.ArrayList;
import java.util.List;

public class  Constants {

    // Chuong
    public static final String PATH_IMG = "src/main/webapp/resources/img/";
    public static final String PATH_OUT_IMG = "src/main/webapp/resources/output_image/";
    public static final String PATH_PYTHON = "src/main/python/main.py";

}
