/*
 * @author: ChuongHV1
 * @date: Nov 26, 2021
 */
package fa.appcode.web.controller;

import fa.appcode.common.logging.LogUtils;
import fa.appcode.common.utils.Constants;
import fa.appcode.services.ImageService;
import fa.appcode.web.DTO.ResponseObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import org.apache.commons.io.FileUtils;

@Controller
@RequestMapping("/")
public class InitController {

    @Autowired
    private ImageService imageService;


    @GetMapping("index")
    public String showIndex() {
        return "index";
    }

    @PostMapping(value = "api/upload/image")
    public ResponseEntity<ResponseObject> saveEmployee(
            @RequestParam(name = "file", required = false) MultipartFile file) throws IOException {

        LogUtils.getLogger().info("UPload file done");
        String fileName = null;

        if (file != null && !file.isEmpty()) {
            String storageFolder = Constants.PATH_IMG;
            try {
                FileUtils.cleanDirectory(new File(storageFolder));
                FileUtils.cleanDirectory(new File(Constants.PATH_OUT_IMG));
                fileName = imageService.storeFile(file, storageFolder);
                Path currentRelativePath = Paths.get("");
                String s = currentRelativePath.toAbsolutePath().toString();
                LogUtils.getLogger().info(s);
                String command = "conda run python " + "~/Downloads/Demo/HarDNet-MSEG/Test.py";
                Process p = Runtime.getRuntime().exec(command);
                p.waitFor();
                LogUtils.getLogger().info(command);
            } catch (Exception e) {
                return ResponseEntity.status(HttpStatus.NOT_IMPLEMENTED)
                        .body(new ResponseObject("FAIL",
                                "Save fail",
                                ""));
            }

        }

        return ResponseEntity.status(HttpStatus.OK)
                .body(new ResponseObject("SUCCESS",
                        "Save successfully",
                        fileName));
    }
}
